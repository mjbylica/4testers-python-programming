friend = {
    "name": "Monia",
    "age": "40",
    "hobbies": ["rower", "jedzenie"]
}

print(friend["age"])
friend['city'] = "Warszawa"

print(friend)

print(friend["hobbies"][-1])

friend["hobbies"].append("jogging")
print(friend)


### Homework ###
def get_sum_of_list(list_with_numbers):
    return sum(list_with_numbers)


jump_ratings = [1, 2.5, 4, 3]

print(get_sum_of_list(jump_ratings))

def get_average_of_two_num(no1,no2):
    return (no1+no2)/2

print(get_average_of_two_num(2,3))

january = [-4, 1.0, -7, 2]
february = [-13, -9, -3, 3]

average_temperature = get_average_of_two_num(get_sum_of_list(january), get_sum_of_list(february))

print(average_temperature)




def get_player_profile(dict):
    return print(f"The player {dict['nick']} is of type {dict['type']} and has {dict['exp_points']} EXP")

player = {
    "nick": "maestro-54",
    "type": "warrior",
    "exp_points": 3000
}

get_player_profile(player)