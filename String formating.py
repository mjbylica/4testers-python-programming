def print_hello_sign(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}")


print_hello_sign("Michał", "Toruń")
print_hello_sign("Beata", "Gdynia")


def get_email_for_user(fname, surname):
    return f"{fname.lower()}.{surname.lower()}@4testers.pl"


print(get_email_for_user("Janusz", "Nowak"))
