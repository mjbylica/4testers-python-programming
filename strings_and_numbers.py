# F-string
first_name = "Marta"
last_name = "Bylica"
email = "mjbylica@gmail.com"

my_bio = f"Cześć, jestem {first_name} {last_name}.\nNapisz do mnie pod {email}"
print(my_bio)

print("Cześć, jestem ", first_name, last_name, ". Napisz do mnie pod " email)

### Algebra##

def circle_area( circle_radius ):
    return 3.14*circle_radius**2

circle_radius = 5

area_result = circle_area( circle_radius )
print(area_result)

circle_circumference = 2 * 3.14 * circle_radius
circle_circumference = "{:.2f}".format(circle_circumference)
print(circle_circumference)
