def upper_word(word):
    return word.upper()


big_dog = upper_word("dog")
print(big_dog)


def add_two_numbers(a, b):
    return a + b


no_1 = 1
no_2 = 2
result = add_two_numbers(no_1, no_2)
print(result)


def get_number_squared(num):
    return num ** 2


power_result = get_number_squared(0)
print(power_result)

power_result = get_number_squared(16)
print(power_result)

power_result = get_number_squared(2.55)
power_result = "{:.2f}".format(power_result)
print(power_result)


def cuboid_volume(d, e, f):
    return d * e * f


volume = cuboid_volume(3, 5, 7)
print(volume)


def print_carbrand():
    print("Mazda")


print_carbrand()


### Celcius exercise ###

def converting_Celsius_to_Farenhaits(celsius):
    return celsius * 9 / 5 + 32


Celsius_to_Farenhaits = converting_Celsius_to_Farenhaits(20)
print(Celsius_to_Farenhaits)
